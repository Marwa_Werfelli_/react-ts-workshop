import React, { useState } from "react";
type AuthUser = {
  name: string;
  email: string;
};
const LoggedIn = () => {
  const [isLoggedIn, setIsLoggedIn] = useState<boolean | null>(null);
  //   const [user, setUser] = useState<AuthUser | null>(null);
  //wont be null
  const [user, setUser] = useState<AuthUser>({} as AuthUser);

  const handleLogin = () => {
    setIsLoggedIn(true);
    setUser({
      name: "marwa",
      email: "marwa@gmail.com",
    });
  };
  const handleLogout = () => {
    setIsLoggedIn(false);
    // setUser(null);
  };
  return (
    <div>
      <button onClick={handleLogin}>Login</button>
      <button onClick={handleLogout}>Logout</button>
      <div>
        {user?.name} is {isLoggedIn ? "logged in" : "logged out"}
      </div>
    </div>
  );
};

export default LoggedIn;
