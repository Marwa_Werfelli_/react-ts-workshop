import React from "react";
type StatusProps = {
  status: "loading" | "success" | "failed";
};
const Status = (props: StatusProps) => {
  let message;
  if (props.status === "loading") {
    message = "Loading...";
  } else if (props.status === "success") {
    message = "fetched successfully";
  } else if (props.status === "failed") {
    message = "failed";
  }
  return <div>{message}</div>;
};

export default Status;
