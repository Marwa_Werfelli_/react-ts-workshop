export type ButtonProps = {
  handleClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
  //   children: string;
};
