import React from "react";
import { ButtonProps } from "./Button.type";
// type ButtonProps = {
//   handleClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
// };
const Button = ({ handleClick }: ButtonProps) => {
  return <button onClick={(event) => handleClick(event)}>Click </button>;
};

export default Button;
