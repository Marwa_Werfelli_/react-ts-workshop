import React from "react";

type Props = {
  value: string;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

type InputProps = Props & React.ComponentProps<"input">;

const Input = (props: InputProps) => {
  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e);
  };
  return <input onChange={handleInputChange} {...props} />;
};

export default Input;
