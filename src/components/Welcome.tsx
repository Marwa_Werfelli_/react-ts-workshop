import React from "react";
import { WelcomeProps } from "./Welcome.types";

// type WelcomeProps = {
//   name: {
//     first: string;
//     last: string;
//   };
//   lessonsCount?: number;
//   isAuthentificated: boolean;
//   lessons: { name: string; time: string }[];
//   children: React.ReactNode;
//   styles: React.CSSProperties;
// };
const Welcome = (props: WelcomeProps) => {
  const { lessonsCount = 0 } = props;
  return (
    <div style={props.styles}>
      <h1>
        {props.isAuthentificated
          ? `Welcome ${props.name.first} ${props.name.last}! you have ${lessonsCount} lessons today`
          : `Welcome Guest`}
      </h1>
      <h3>Lessons</h3>
      {props.lessons.map((lesson) => (
        <div>
          {lesson.name} at {lesson.time}
        </div>
      ))}
      {props.children}
    </div>
  );
};

export default Welcome;
