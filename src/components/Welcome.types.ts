type Name = {
  first: string;
  last: string;
};
export type WelcomeProps = {
  name: Name;
  lessonsCount?: number;
  isAuthentificated: boolean;
  lessons: { name: string; time: string }[];
  children: React.ReactNode;
  styles: React.CSSProperties;
};
