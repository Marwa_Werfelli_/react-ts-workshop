import "./App.css";
import Button from "./components/Button";
import Input from "./components/Input";
import LoggedIn from "./components/LoggedIn";
import RandomNumber from "./components/RandomNumber";
import Status from "./components/Status";
import Welcome from "./components/Welcome";

function App() {
  return (
    <div className="App">
      <LoggedIn />
      <Welcome
        name={{ first: "marwa", last: "werfelli" }}
        lessonsCount={3}
        isAuthentificated={true}
        lessons={[
          { name: "Math", time: "08-09" },
          { name: "Eng", time: "18-19" },
        ]}
        styles={{ background: "red" }}
      >
        <Status status="loading" />
      </Welcome>
      <Button handleClick={(e) => console.log(`hey ${e}`)} />
      <Input value="" handleChange={(e) => console.log(e.target.value)} />
      <RandomNumber value={10} isPositive />
    </div>
  );
}

export default App;
